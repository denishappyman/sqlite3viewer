#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QDate>
#include <QDebug>
#include <QStringList>
#include <QMessageBox>
#include "database.h"


DataBase::~DataBase()
{
  closeDataBase();
}

bool DataBase::connectToDataBase(const QString &fileName)
{
  return openDataBase(fileName);
}

QStringList DataBase::tables()
{
  return db.tables(QSql::Tables);
}

bool DataBase::openDataBase(const QString &fileName)
{
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(fileName);
  db.open();
  if(db.isOpen()){
    qDebug() << "connection ok";
    return true;
  } else {
     QMessageBox::critical(nullptr, "Error", "DB connection failed");
     qDebug() << "DB connection failed";
    return false;
  }
}

void DataBase::closeDataBase()
{
  db.close();
}


