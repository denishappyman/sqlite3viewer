#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>

class DataBase
{

public:
  DataBase() = default;
  ~DataBase();
  bool connectToDataBase(const QString &fileName);
  QStringList tables();
private:
  bool openDataBase(const QString &fileName);
  void closeDataBase();

private:
  QSqlDatabase    db;
};

#endif // DATABASE_H
