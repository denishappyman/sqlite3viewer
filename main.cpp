#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include "database.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  DataBase *db = new DataBase;

  MainWindow w(nullptr, db);

  w.show();

  return a.exec();
}
