#include "mainwindow.h"
//#include <QSqlTableModel>
#include <QTableView>
#include "ui_mainwindow.h"
#include "database.h"
#include "sqltablemodel.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
}

MainWindow::MainWindow(QWidget *parent, DataBase *db) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _db(db),
  _tableHeaders(QStringList() << QString("Время")
                              << QString("Значение")
                              << QString("Группа"))
{
  ui->setupUi(this);
  QString fileName = QFileDialog::getOpenFileName(this, "Open sqlite3 dataBase", "", "");
  if(db->connectToDataBase(fileName))
  {
    _model = new SqlTableModel(this);
    configure(ui->customPlot);
    configure(_model, db->tables().front());
    configure(ui->tableView, _model);
    configure(ui->comboBox, db->tables());
  }
}

MainWindow::~MainWindow()
{
  if(_model)
  {
    delete _model;
  }
  if (_db)
  {
    delete _db;
  }
  delete ui;
}

void MainWindow::replotData(QCustomPlot *customPlot)
{
  QVector<QCPGraphData> timeData(_model->rowCount());

  for (int i = 0; i < _model->rowCount(); ++i)
  {
    timeData[i].key = QDateTime::fromString(_model->index(i, _model->fieldIndex("Time"))
                                            .data().toString()
                                            , SqlTableModel::format)
        .toTime_t();
    timeData[i].value =
        _model->index(i, _model->fieldIndex("Value")).data().toDouble();
  }

  double Tmin =
      (*timeData.begin())
      .key;
  double Tmax =
      (*std::prev(timeData.end()))
      .key;

  double Ymin =
      (*std::min_element(timeData.begin(), timeData.end(),
                         [](const QCPGraphData &x, const QCPGraphData &y)
  {
    return x.value < y.value;
  }))
      .value;

  double Ymax =
      (*std::max_element(timeData.begin(), timeData.end(),
                         [](const QCPGraphData &x, const QCPGraphData &y)
  {
    return x.value < y.value;
  }))
      .value;

  customPlot->xAxis->setRange(Tmin, Tmax);
  customPlot->yAxis->setRange(Ymin, Ymax);
  customPlot->graph(0)->data()->set(timeData);
  customPlot->replot();
}

void MainWindow::configure(QCustomPlot *customPlot)
{
  customPlot->addGraph();
  customPlot->xAxis->setLabel("Время");
  customPlot->yAxis->setLabel("Значение");
  QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);

  dateTicker->setDateTimeFormat("hh:mm:ss");
  customPlot->xAxis->setTicker(dateTicker);
  replotData(ui->customPlot);
}

void MainWindow::configure(SqlTableModel *model, const QString &tableName)
{

  onTableChosen(tableName);

  for(int i = 0, j = 0; i < _model->columnCount(); i++, j++)
  {
    model->setHeaderData(i,Qt::Horizontal,_tableHeaders[j]);
  }
}

void MainWindow::configure(QTableView *tableView, SqlTableModel *model)
{
 tableView->setModel(model);
 tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
 tableView->setSelectionMode(QAbstractItemView::SingleSelection);
 tableView->resizeColumnsToContents();
 tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
 tableView->horizontalHeader()->setStretchLastSection(true);
}

void MainWindow::configure(QComboBox *comboBox, const QStringList &tables)
{
  comboBox->addItems(tables);
  connect(ui->comboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(onTableChosen(QString)));
}

void MainWindow::onTableChosen(const QString &tableName)
{
  _model->setTable(tableName);
  _model->select();
  while(_model->canFetchMore())
  {
    _model->fetchMore();
  }
  ui->tableView->resizeColumnsToContents();
  replotData(ui->customPlot);
}
