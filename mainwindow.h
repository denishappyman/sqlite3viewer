#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class SqlTableModel;
class QTableView;
class QComboBox;
class DataBase;
class QCustomPlot;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  MainWindow(QWidget *parent = 0, DataBase *db = nullptr);
  ~MainWindow();

private slots:
  void onTableChosen(const QString &tableName);

private:
  void configure(SqlTableModel * model, const QString &tableName);
  void configure(QCustomPlot *customPlot);
  void configure(QTableView *tableView, SqlTableModel *model);
  void configure(QComboBox *comboBox, const QStringList &tables);
  void replotData(QCustomPlot *customPlot);

  Ui::MainWindow *ui;
  DataBase *_db;
  SqlTableModel  *_model;
  QStringList _tableHeaders;
};

#endif // MAINWINDOW_H
