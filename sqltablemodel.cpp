#include "sqltablemodel.h"
#include <QDateTime>

const QString SqlTableModel::format = "dd.MM.yyyy hh:mm:ss";

SqlTableModel::SqlTableModel(QObject * parent, QSqlDatabase db) :
QSqlTableModel(parent, db)
{

}

QVariant SqlTableModel::data(const QModelIndex& index, int role) const {

  if (role!=Qt::DisplayRole) {
      return QSqlTableModel::data(index,role);
  }
  if (index.column() != fieldIndex("Time")) {
      return QSqlTableModel::data(index,role);
  }
  QVariant value = QDateTime::fromSecsSinceEpoch(QSqlQueryModel::data(index, role).toInt()).toString(format);
  return value;
}
