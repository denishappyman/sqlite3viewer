#ifndef SQLTABLEMODEL_H
#define SQLTABLEMODEL_H
#include <QSqlTableModel>

class SqlTableModel : public QSqlTableModel
{
public:
  static const QString format;
  explicit SqlTableModel(QObject * parent = 0, QSqlDatabase db = QSqlDatabase() ); // the same arguments as QSqlTableModel
  QVariant data(const QModelIndex &item, int role = Qt::DisplayRole) const;
};

#endif // SQLTABLEMODEL_H
